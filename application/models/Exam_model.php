<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Exam_model extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	public function nameSave($name)
	{
			$data['name'] = $name ;
			if($this->db->insert('users',$data))
			{
				$id =  $this->db->insert_id();
				$this->session->set_userdata('userId', $id);
				return 200;
			}else{
				return 401 ; 	
			}
	}
	public function getData()
	{
		$this->db->select('id');
		$query = $this->db->get_where('poll_test');
		$res['total_votes']  = $query->num_rows();
		return $res ; 
	}
	
	public function getQue($id,$inc=null)
	{
		$sql = "SELECT id,qn FROM `que` WHERE `id` = '".$id."' " ; 
		$query = $this->db->query($sql);
		$res['qns'] =  $query->row_array();
		$res['inc'] =  $inc ;
		return $res ; 
	}
	public function selectedSave($val,$id)
	{
		$this -> db -> where('user_id', $this->session->userdata('userId'));
		$this -> db -> where('qn_id', $id);
		$this -> db -> delete('result');
		$sql = "select id from ans where qn_id='".$id."' AND ans='".$val."'" ;
		$query = $this->db->query($sql);
		$count =  $query->num_rows();
		$data['result'] = $count ;
		$data['qn_id'] = $id ;
		$data['user_id'] =  $this->session->userdata('userId') ;
		$this->db->insert('result',$data) ; 
	}
	public function result($result)
	{
		$sql = "select id from result where user_id='".$this->session->userdata('userId')."' AND result='".$result."' " ;
		$query = $this->db->query($sql);
		return  $query->num_rows();
		
	}
	public function getSingleField(){
		$this->db->select('name');
		$que = array('id'=>$this->session->userdata('userId')); 
		$query = $this->db->get_where('users',$que);
		return $query->row()->name;
	}
	public function options($id)
	{
		$sql = "select options from options where qn_id='".$id."'" ;
		$exec = $this->db->query($sql);
		return  $exec->result_array();
		
	}
}
?>