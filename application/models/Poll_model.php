<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Poll_model extends CI_Model {
	function __construct() {
		parent::__construct();
	}
	public function savePoll()
	{
		extract($_POST) ;
		if(count($_POST)==0){
			return 403 ; 
		}else{
			$data['vote'] = $performance ;
			if($this->db->insert('poll_test',$data))
			{
				return 200;
			}else{
				return 401 ; 	
			}
		}
	}
	public function getData()
	{
		$this->db->select('id');
		$query = $this->db->get_where('poll_test');
		$res['total_votes']  = $query->num_rows();
		return $res ; 
	}
	
	public function getDataByVotes($vote)
	{
		$sql = "SELECT id FROM `poll_test` WHERE `vote` = '".$vote."' " ; 
		$query = $this->db->query($sql);
		$res =  $query->num_rows();
		return $res ; 
	}
}
?>