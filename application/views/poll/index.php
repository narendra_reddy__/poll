<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<?php //echo "<pre>";  print_r($votes) ;die; ?>
<style>
	body { margin-top:20px; }
	.panel-body:not(.two-col) { padding:0px }
	.glyphicon { margin-right:5px; }
	.glyphicon-new-window { margin-left:5px; }
	.panel-body .radio,.panel-body .checkbox {margin-top: 0px;margin-bottom: 0px;}
	.panel-body .list-group {margin-bottom: 0;}
	.margin-bottom-none { margin-bottom: 0; }
	.panel-body .radio label,.panel-body .checkbox label { display:block; }
	.panel-heading {
		padding: 1px 15px;
		background: #5b99be;
		color: #fff;
	}
	.panel-body{margin: 10px 10px 10px 0px;}
</style>
<div class="container">
	<div class="row">
	<?php if($this->session->flashdata("user_success")!=""){?>
			<div class="myAlert-top alert alert-success alert-dismissible col-md-6" role="alert">
				<?php echo $this->session->flashdata("user_success");?>	<i class="fa fa-thumbs-up"></i>
			</div>
		<?php }?>
		<?php if($this->session->flashdata("user_error")!=""){ ?>
			<div class="myAlert-top alert alert-danger alert-dismissible col-md-6" role="alert">
				<?php echo $this->session->flashdata("user_error");?> 
				<span class="imprnt"  > <i class="fa fa-exclamation-circle"></i> </span>
			</div>
		<?php }?>
	</div>
    <div class="row">
        <div class="col-md-3">
            <div class="panel panel-primary">
			<form action="<?= base_url();?>poll/save" method="post" >
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <span class="glyphicon glyphicon-arrow-right"></span>Poll Test<span>
                    </h3>
                </div>
                <div class="panel-body">
					
						<ul class="list-group">
							<li class="list-group-item">
								<div class="radio">
									<label>
										<input type="radio" name="performance" value="1" >
										Good
									</label>
								</div>
							</li>
							<li class="list-group-item">
								<div class="radio">
									<label>
										<input type="radio" name="performance" value="2">
										Excellent
									</label>
								</div>
							</li>
							<li class="list-group-item">
								<div class="radio">
									<label>
										<input type="radio" name="performance" value="3" >
										Bad
									</label>
								</div>
							</li>
							<li class="list-group-item">
								<div class="radio">
									<label>
										<input type="radio" name="performance" value="4" >
										Can Be Improved
									</label>
								</div>
							</li>
							<li class="list-group-item">
								<div class="radio">
									<label>
										<input type="radio" name="performance" value="5" >
										No Comment
									</label>
								</div>
							</li>
						</ul>
					
                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-primary btn-sm">
                        Vote
					</button>
                    <a href="#">View Result</a></div>
            </div>
			</form>
        </div>
     
		<div class="span6">
			  <h5>Poll Result</h5>
			  <strong>Good</strong><span class="pull-right"> 
				<?php 
					$goodCount = $this->poll_model->getDataByVotes(1) ;
					$perG = number_format(($goodCount*100)/$total_votes,2) ; 
					echo $perG ; 
				 ?>%
			  </span>
			  <div class="progress progress-danger active">
				  <div class="bar" style="width: <?= $perG; ?>%"></div>
			  </div>
			  
			  <strong>Excellent</strong><span class="pull-right">
				<?php 
					$exCount = $this->poll_model->getDataByVotes(2) ;
					$perE = number_format(($exCount*100)/$total_votes,2) ; 
					echo $perE ; 
				 ?>%
			  </span>
			  <div class="progress progress-info active">
				   <div class="bar" style="width: <?= $perE; ?>%"></div>
			  </div>
			  
			  <strong>Bad</strong><span class="pull-right">
				<?php 
					$badCount = $this->poll_model->getDataByVotes(3) ;
					$perB = number_format(($badCount*100)/$total_votes,2) ; 
					echo $perB ; 
				 ?>%
			  </span>
			  <div class="progress progress-striped active">
				  <div class="bar" style="width: <?= $perB; ?>%"></div>
			  </div>
			  
			  <strong>Can Be Improved</strong><span class="pull-right">
				<?php 
					$impCount = $this->poll_model->getDataByVotes(4) ;
					$perI = number_format(($impCount*100)/$total_votes,2) ; 
					echo $perI ; 
				 ?>%
			  </span>
			  <div class="progress progress-success active">
				  <div class="bar" style="width: <?= $perI; ?>%"></div>
			  </div>
			  
			  <strong>No Comment</strong><span class="pull-right">
				<?php 
					$noCount = $this->poll_model->getDataByVotes(5) ;
					$perNo = number_format(($noCount*100)/$total_votes,2) ; 
					echo $perNo ; 
				 ?>%
			  </span>
			  <div class="progress progress-warning active">
				  <div class="bar" style="width: <?= $perNo; ?>%"></div>
			  </div>
		</div>
    </div>
	

</div>
<script>
	$(document).ready(function(){
		$(".myAlert-").show();
		  setTimeout(function(){
			$(".myAlert-top").hide(); 
		}, 5000);
	});
</script>