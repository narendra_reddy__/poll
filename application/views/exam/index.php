<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<style>
		@import url('https://fonts.googleapis.com/css2?family=Montserrat&display=swap');

* {
    margin: 0;
    padding: 0;
    box-sizing: border-box
}

body {
    background-color: #333
}

.container {
    background-color: #555;
    color: #ddd;
    border-radius: 10px;
    padding: 20px;
    font-family: 'Montserrat', sans-serif;
    max-width: 700px
}

.container>p {
    font-size: 32px
}

.question {
    width: 75%
}

.options {
    position: relative;
    padding-left: 40px
}

#options label {
    display: block;
    margin-bottom: 15px;
    font-size: 14px;
    cursor: pointer
}

.options input {
    opacity: 0
}

.checkmark {
    position: absolute;
    top: -1px;
    left: 0;
    height: 25px;
    width: 25px;
    background-color: #555;
    border: 1px solid #ddd;
    border-radius: 50%
}

.options input:checked~.checkmark:after {
    display: block
}

.options .checkmark:after {
    content: "";
    width: 10px;
    height: 10px;
    display: block;
    background: white;
    position: absolute;
    top: 50%;
    left: 50%;
    border-radius: 50%;
    transform: translate(-50%, -50%) scale(0);
    transition: 300ms ease-in-out 0s
}

.options input[type="radio"]:checked~.checkmark {
    background: #21bf73;
    transition: 300ms ease-in-out 0s
}

.options input[type="radio"]:checked~.checkmark:after {
    transform: translate(-50%, -50%) scale(1)
}

.btn-primary {
    background-color: #555;
    color: #ddd;
    border: 1px solid #ddd
}

.btn-primary:hover {
    background-color: #21bf73;
    border: 1px solid #21bf73
}

.btn-success {
    padding: 5px 25px;
    background-color: #21bf73
}

@media(max-width:576px) {
    .question {
        width: 100%;
        word-spacing: 2px
    }
}
</style>
	<div class="container mt-sm-5 my-1"  id="resultDiv" style="display:none" >
		
	</div> 
	<div class="container mt-sm-5 my-1"  id="nameDiv" >
		<input type="text"  id="nameVal"  > <button type="button" onclick="nameSave()"  class="btn btn-success" >Submit </button>
	</div> 
<div class="container mt-sm-5 my-1 " id="appendQue"  style="display:none" >
	<?php $this->load->view("exam/qu"); ?> 
</div>

<script>
	function callNextQue(id,checked){
		if(checked==1){
			if($("input:radio[name='radioQue']").is(":checked")) { 
			}else{
				alert("Please select at leaset one") ; 
				return false ;
			}
		}
		var base_url = '<?= base_url() ; ?>';
			$.ajax({
				url:base_url+'exam/append?queId='+parseInt(id),
				dataType:"json",
				contentType: false,
				cache: false,
				type: 'POST',
				processData: false,
				success:function(data)
				{	
					$("#appendQue").html(data.body);
				},
				error:function()
				{
				  alert("Error")
				}
			});
	}
	
	function nameSave(){
		$("#appendQue").show();
		$("#nameDiv").hide();
		var base_url = '<?= base_url() ; ?>';
		var name = $("#nameVal").val() ; 
			$.ajax({
				url:base_url+'exam/nameSave?name='+name,
				dataType:"json",
				contentType: false,
				cache: false,
				type: 'POST',
				processData: false,
				success:function(data)
				{	
					console.log('suc')
				},
				error:function()
				{	
					
				}
			});
	}
	function selectedSave(val , id){
		var base_url = '<?= base_url() ; ?>';
		$.ajax({
				url:base_url+'exam/selectedSave?val='+val+'&id='+id,
				dataType:"json",
				contentType: false,
				cache: false,
				type: 'POST',
				processData: false,
				success:function(data)
				{	
					console.log('suc')
				},
				error:function()
				{	
					
				}
			});
	}
	function finish(){
		$("#appendQue").hide();
		$("#nameDiv").hide();
		$("#resultDiv").show();
		var base_url = '<?= base_url() ; ?>';
			$.ajax({
				url:base_url+'exam/result',
				dataType:"json",
				contentType: false,
				cache: false,
				type: 'POST',
				processData: false,
				success:function(data)
				{	
					$("#resultDiv").html(data.body);
				},
				error:function(data)
				{
					console.log(data.body) ;
					$("#resultDiv").html(data.body);
				}
			});
	}
</script>