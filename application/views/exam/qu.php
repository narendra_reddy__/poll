   <?php 
		if($inc!=null){
			$nxtId = $inc+1 ; 
			$this->session->set_userdata('inc', 2);
		}else{
			$nxtId = $this->session->userdata('inc')+1 ; 
			$this->session->set_userdata('inc', $nxtId);
		}
	?>
	<div class="question ml-sm-5 pl-sm-5 pt-2">
        <div class="py-2 h5"><b>Q. <?= $qns['qn'] ; ?></b></div>
        <div class="ml-md-3 ml-sm-3 pl-md-5 pt-sm-0 pt-3" id="options"> 
			
			<?php
				$res = $this->exam_model->options($qns['id']) ; 
				//echo "<pre>"; print_r($res) ; die; 
				$i = 1 ; 
				foreach($res as $opti){
			?>
				<label class="options"><?= $opti['options'] ; ?> 
					<input type="radio" name="radioQue" value="<?= $i ; ?>" onclick="selectedSave(<?= $i ; ?>,'<?= $qns['id'];?>')" > <span class="checkmark"  ></span> 
				</label> 
			<?php $i++ ;  } ?> 
		</div>
    </div>
	<?php if($nxtId<11){ ?>
    <div class="d-flex align-items-center pt-3">
        <div id="prev"> <button class="btn btn-primary" onclick= "callNextQue('<?php echo $nxtId ; ?>',2)" >Skip</button> </div>
        <div class="ml-auto mr-sm-5"> <button class="btn btn-success" onclick= "callNextQue('<?php echo $nxtId ; ?>',1)" >Next</button> </div>
    </div>
	<?php }else{?>
		<div class="ml-auto mr-sm-5"> <button class="btn btn-success" onclick= "finish()" >Finish</button> </div>
	<?php } ?>
</div>
